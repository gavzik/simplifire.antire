simpliFiRE.AntiRE - An Executable Collection of Anti-Reversing Techniques
=========================================================================

Description
-----------

Reverse engineering is a methodology used to recover high-level information about structure and semantics of low-level analysis subjects. In the context of software, it is applied e.g. to establish understanding of embedded protocols and algorithms in order to achieve interoperability with software that is only available in compiled, binary form. This procedure is not always desired by creators of software as their products may contain intellectual property that they want to guard from competitors. Thus, protection schemes for binary code have been developed since decades. 
 
Nowadays, such protection schemes are massively applied to malicious software (malware). The main reason is that the modifications performed on the binary code allow evasion from detection by security products. As a side effect, if the employed techniques or their effects are not known to malware analysts performing reverse engineering on malware samples, the analysis time can increase dramatically.

AntiRE is a collection of such anti analysis approaches, gathered from various sources like Peter Ferrie's [The "Ultimate" Anti-Debugging Reference](http://pferrie.host22.com/papers/antidebug.pdf) and Ange Albertini's [corkami](http://corkami.com). While these techniques by themselves are nothing new, we believe that the integration of these tests in a single, executable file provides a comprehensive overview on these, suitable for directly studying their behaviour in a harmless context without additional efforts. AntiRE includes different techniques to detect or circumvent debuggers, fool execution tracing, and disable memory dumping. Furthermore, it can detect the presence of different virtualization environments and gives examples of techniques used to twarth static analysis.

The tests are implemented as isolated, minimal examples and accompanied by descriptions explaining their mode of operation. To provide a better overview, they are organized in groups of similar techniques. When executed in a debugger, tests can be easily accessed through a binary "table of contents", realized as a long sequence of commented function calls. When executed, our tool generates output to a console window, showing the descriptions and a test result. This can be used to quickly test an analysis environment for robustness against the included mechanisms.

Instructions
------------

Some of the code fragments in this tool may trigger signatures and heuristics as used by many AntiVirus solutions. Because of this, we included the executable and source code of the binary release in another zip-file, password-protected with the keyword "novirus".

The tool is intended to be used with a debugger. All individual tests will produce output on the console, giving a short explanation and showing the test result in the end. The tests are grouped by similar features and easily accessible via a "menu".

There are also some commandline options:
-  -a   automated execution (without keyboard interrupts)
-  -o   output report to file ./anti_re_output.log
  
The automated execution can be used to perform tests against analysis environments in order to give hints where improvement is necessary to dodge the techniques.

Final Words
-----------

Many of the techniques implemented interact in a very special way with the operating system and can cause undesired effects. By using this tool,we assume that you know what you are doing and you accept that you are using it on your own risk. As stated in the license, we will not take liability for any damage caused by this tool.

Publications
------------

Daniel Plohmann and Christopher Kannen  
[AntiRE - An Executable Collection of Anti-Reversing Techniques](http://www.sec.t-labs.tu-berlin.de/spring/content/spring7_16_abstract_plohmann.pdf "AntiRE - An Executable Collection of Anti-Reversing Techniques")  
In: Collin Mulliner, Patrick Stewin, editors, Proceedings of the Seventh GI SIG SIDAR Graduate Workshop on Reactive Security (SPRING). Technical Report SR-2012-01. GI FG SIGAR, Berlin, July 2012.  
